

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyD7SKIPfnbbAkEjZNdncIxXPNYB4E-WJ7I",
    authDomain: "administradorweb-8bc3f.firebaseapp.com",
    projectId: "administradorweb-8bc3f",
    storageBucket: "administradorweb-8bc3f.appspot.com",
    messagingSenderId: "280663684556",
    appId: "1:280663684556:web:6ba7d1354e788859fe5969"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  export {app}
  export const auth = getAuth(app);
  export const storage = getStorage(app)

